
Test USB 

    YZXSTUDIO ZY1280E 
    Oscilloscope USB Power Meter Power Delivery Analyzer Tester

USB V4, Thunderbolt v3

    EJ903W

    The RT1710S is a Type-C cable ID for active and passive cables. 
    All USB Full-Featured Type-C cables shall be electronically marked. 
    Electronically marked cables shall 
    support USB Power Delivery Structured VDM Discover Identity command directed to SOP′. 
    This provides a method to determine the characteristics of the cable, 
    e.g. its current carrying capability, 
    its performance, vendor identification, etc. 
    This may be referred to as the USB Type-C Cable ID function.


