

https://youtu.be/0z6AhrOSrRs


2 years ago
------------ TIME STAMP -------------


COURSE 1 
MATHEMATICS FOR MACHINE LEARNING:LINEAR ALGEBRA

INTRODUCTION TO LINEAR ALGEBRA AND TO MATHEMATICS FOR MACHINE LEARNING
0:00:00 Introduction Solving data science challenges with mathemaatics
0:02:27 Motivations for linear algebra
0:05:57 Getting a handle on vectors
0:15:03 Operations with vectors
0:26:32 Summary 

VECTORS ARE OBJECTS THAT MOVE AROUND SPACE
0:27:37 Introduction to module 2 - Vectors
0:28:27 Modulus & inner product 
0:38:28 Cosine & dot product 
0:44:21 Project 
0:51:09 changing basis
1:02:34 Basis, Vector space, and linear independence
1:06:47 Application of changing basis
1:10:16 Summary 

MATRICES IN LINEAR ALGEBRA:OBJECTS THAT OPERATE ON VECTORS
1:11:36 Matrices, Vectors, and solving simultaneous equation problems
1:17:08 How matrices transform space
1:22:49 Types of matrix transformation
1:31:28 Composition or combination of matrix transformations
1:40:28 Solving the apples and bananas problem Gaussian elimination
1:48:29 Going from Gaussian elimination to finding the inverse matrix
1:57:07 Determinants and inverses
2:07:44 Summary

MATRICES MAKE LINEAR MAPPINGS
2:08:43 Introduction Eintein summation convention and the symmetry of the dot product
2:18:37 Matrices changing basis
2:29:52 Doing a transformation in a changed basis
2:34:30 Orthogonal matrices
2:41:10 The Gram-Schmidt process
2:47:18 Example Reflecting in a plane

EIGENVALUES AND EIGENVECTORS:APPLICATION TO DATA PROBLEMS
3:01:28 Welcome to Module 5
3:02:20 What are eigenvalues and eigenvectors
3:06:45 Special eigen-cases
3:10:17 Calculating eigenvectors
3:20:25 Changing to the eigenbasis
3:26:17 Eigenbasis example
3:33:43 Introduction to PageRank
3:42:27 Summary 
3:43:42 Wrap up of this linear algebra course

----------------------------------------------

The second course, Multivariate Calculus, builds on this to look at how to optimize fitting functions to get good fits to data.
It starts from introductory calculus and then uses the matrices and vectors from the first course to look at data fitting.

Course 2
MULTIVARIATE CALCULUS

WHAT IS CALCULUS?
3:45:39 Welcome to Multivariate Calculus
3:47:29 Welcome to Module 1
3:48:33 Functions
3:52:51 Rise Over Run
3:57:48 Definition of a derivative
4:08:30 Differentiation example & special cases
4:16:19 Product rule
4:20:27 Chain rule
4:25:50 Taming a beast
4:31:29 See you next module!

MULTIVARIATE CALCULUS
4:32:09 Welcome to Module2!
4:33:13 Variables, constants & context 
4:41:09 Differentiate with respect to anything
4:45:53 The Jacobian
4:51:42 Jacobian applied
4:58:05 The Sandpit
5:02:48 The Hessian
5:08:27 Reality in hard
5:13:04 See you next module!

MULTIVARIATE CHAIN RULE AND ITS APPLICATIONS
5:13:28 Welcome to Module 3!
5:14:04 Multivariate chain rule
5:16:43 More multivariate chain rule
5:22:21 Simple neural networks
5:28:13 More simple neural networks
5:32:25 See you next module!

TAYLOR SERIES AND LINEARISATION
5:32:59 Welcome to Module!
5:33:35 Building approximate functions
5:37:03 Power Series
5:40:41 Power series derivation
5:49:50 Power series datails
5:56:04 Examples
6:01:24 Linearisation
6:06:41 Multivariate Taylor
6:13:08 See you next module!

INTRO TO OPTIMISATION
6:13:36 Welcome to Module 5!
6:21:51 Gradient Descent 
6:30:58 constrianed optimisation
6:39:32 See you next module!

REGRESSION
6:41:40 simple linear regression
6:51:52 General non linear least squares
6:59:05 Doing least squares regression analysis in practice
7:05:24 Wrap up of this course

-------------------------------------------------------


The third course, Dimensionality Reduction with Principal Component Analysis,
uses the mathematics from the first two courses to compress high-dimensional data.
This course is of intermediate difficulty and will require Python and numpy knowledge.

COURSE 3
Mathematics for Machine Learning: PCA

STATISTICS OF DATASETS
7:06:12 Introduction to the Course 
7:09:59 Welcome to module 1
7:10:41 Mean of a dataset
7:14:41 Variance of one-dimensional datasets
7:19:36 Variance of higher -dimensional datasets
7:24:52 Effect on the mean
7:29:38 Effect on the (co)variance 
7:33:08 See you next module!

INNER PRODUCTS
7:33:35 Welcome to module 2 
7:35:24 Dot product
7:40:07 Inner product definition
7:45:09 Inner product length of vectors
7:52:17 Inner product distance between vectors
7:55:59 Inner product angles and orthogonality
8:01:41 Inner product of functions and random variables (optional)
8:09:03 Heading for the next module!

ORTHOGONAL PROJECTIONS
8:09:38 Welcome to module 3
8:10:19 Projection onto ID subspaces
8:18:02 Example projection onto ID Subspaces
8:12:28 Projections onto higher-dimensional subspaces
8:30:01 Example projection onto a 2D subspaces
8:33:53 This was module 3!

PRINCIPAL COMPONENT ANALYSIS
8:34:26 Welcome to module 4
8:35:35 Problem setting and PCA objective
8:43:20 Finding the coordinates of the projected data
8:48:49 Reformulation of the objective
8:59:15 Finding the basis vectors that span the principal subspace
9:06:55 Steps of PCA
9:11:02 PCA in high dimensions
9:16:51 Other interpretations of PCA (optional)
9:24:33 Summary of this module 
9:25:16 This was the course on PCA
