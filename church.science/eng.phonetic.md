
Set 1: s, a, t, p. 
Set 2: i, n, m, d. 
Set 3: g, o, c, k. 
Set 4: ck, e, u, r. 
Set 5: h, b, f, ff, l, ll, ss.
Set 6: j, v, w, x.
Set 7: y, z, zz, qu.
Consonant digraphs: ch, sh, th, ng.
Vowel digraphs: ai, ee, igh, oa, oo, ar, or, ur, ow, oi, ear, air, ure, er.
ay, ou, ie, ea, oi, ir, ue, wh, ph, ew, aw, au, oe, a-e.

Split digraphs: e-e, igh, o-e, u-e,
Alternative pronunciations: i, o, a, e, u, c, g, y, ch, er, ow, ie, ea, ou, ey

