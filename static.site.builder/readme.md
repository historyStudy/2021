
My static site builder, transform markdown files to html.



# 2022 10-13

For gitlab Pages, run : 

    python  mda.py

The setting is configured with `config.py`,
the output is going to be ../public/
and gitlab will pick it up and do with `gitlab-ci.yml`

at least install Markdown, jinja2

    pip install Markdown

pip freeze --local # output:

    backcall==0.1.0
    blinker==1.4
    click==8.0.1
    colorama==0.4.4
    commonmark==0.9.1
    decorator==4.4.1
    docutils==0.17.1
    feedgenerator==2.0.0
    ghp-import==2.0.2
    importlib-metadata==4.8.1
    ipython==7.10.2
    ipython-genutils==0.2.0
    jedi==0.15.2
    Jinja2==3.0.1
    Markdown==3.3.4
    MarkupSafe==2.0.1
    mergedeep==1.3.4
    mkdocs==1.2.2
    packaging==21.0
    parso==0.5.2
    pelican==4.7.1
    pexpect==4.7.0
    pickleshare==0.7.5
    pkg-resources==0.0.0
    prompt-toolkit==3.0.2
    ptyprocess==0.6.0
    Pygments==2.10.0
    pyparsing==2.4.7
    python-dateutil==2.8.2
    python-magic==0.4.24
    pytz==2021.3
    PyYAML==5.4.1
    pyyaml-env-tag==0.1
    rich==10.12.0
    six==1.13.0
    traitlets==4.3.3
    typing-extensions==3.10.0.2
    Unidecode==1.3.2
    watchdog==2.1.5
    wcwidth==0.1.7
    zipp==3.5.1


# 2021 01 26

./startbootstrap-clean-blog

    copy from ./download/startbootstrap-clean-blog/dist

folders in ./download

    - genius
    gitlab html page template

    startbootstrap-clean-blog
    - free bootstrap theme

# 2021 01 17

Recently I got tired by Jekyll, Pelican
They all has their own specific quirks.

