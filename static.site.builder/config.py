

post_template =  "./startbootstrap-clean-blog/t-mypost.html"
index_template =  "./startbootstrap-clean-blog/t-index.html"
about_template =  "./startbootstrap-clean-blog/t-about.html"
notebook_template =  "./startbootstrap-clean-blog/t-notebook.html"

sourceTarget = [
        #{"source": "../notebook.history.md",
        #    "target": "../public/notebook.history.html" },

        {"source": "../preface.history.notebook.md",
            "target": "../public/preface.history.notebook.html"},

        {"source": "../gfw.china.ban.md",
            "template": post_template,
            "target": "../public/gfw.china.ban.html"},

        {"source": "../about.md",
            "template": about_template,
            "target": "../public/about.html"},

        #{"source": "../preface.2022.0212.md",
        #    "template": index_template,
        #    "target": "../public/index.html"},
        {"source": "../notebook.history.md",
            "template": notebook_template,
            "target": "../public/index.html"},
        {"source": "../notebook.english.txt",
            "template": notebook_template,
            "target": "../public/notebook.english.html"},

        ]

tmpFile = "/tmp/md0124.html"

