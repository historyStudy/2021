import markdown

# Config file is python file, config.py
import config

fileName = "../notebook.history.md"

postTemplate = "./startbootstrap-clean-blog/t-mypost.html"

tmpFile = "/tmp/md0124.html"


def transferMarkdown():
    """test stage
    """
    html = None

    with open(fileName, 'r') as f:

        text = f.read()
        html = markdown.markdown(text)

        with open(tmpFile, 'w') as w:
            w.write(html)
            pass

        pass
    return html


from jinja2 import Environment, FileSystemLoader, select_autoescape

def fillTemplate():
    env = Environment(
        loader = FileSystemLoader(searchpath = "./"),
        autoescape = select_autoescape(default_for_string = True)
    )

    template = env.get_template(postTemplate)

    html = transferMarkdown()

    renderred = template.render(content  = html)

    return renderred


def mdTextToHtml(text):
    """
    """
    html = None

    if text:
        md = markdown.Markdown(extensions=['toc'])
        html = md.convert(text)

    return html


## 2022-02-13

def mdFileToHtmlString(fileName):
    """
    """
    html = None

    with open(fileName, 'r') as f:
        text = f.read()

        return mdTextToHtml(text)

        #html = markdown.markdown(text)
        #md = markdown.Markdown(extensions=['toc'])
        #html = md.convert(text)

    #return html


def sourceMarkdownToTargetHtml(source=None, target=None, template=None):
    print("Source  : %s \nTarget  : %s \nTemplate: %s \n\n"%(
        source, target, template))

    env = Environment(
        loader = FileSystemLoader(searchpath = "./"),
        autoescape = select_autoescape(default_for_string = True)
    )

    template = env.get_template(template)

    html = mdFileToHtmlString(source)

    # var ? content, title, 
    renderred = template.render(content  = html)

    with open(target, 'w') as f:
        f.write(renderred)

    #return renderred


def testOne():
    for o in config.sourceTarget:

        template = config.post_template
        if 'template' in o:
            template = o['template']

        sourceMarkdownToTargetHtml(
                source=o['source'],
                target=o['target'],
                template=template
                )
    pass


def test2():
    job = config.sourceTarget[2]

    template = config.post_template
    if 'template' in job:
        template = job['template']

    #print(job, template)

    sourceMarkdownToTargetHtml(
            source=job['source'],
            target=job['target'],
            template=template
            )


if __name__ == "__main__":
    #h = fillTemplate()
    #htmlFile = "./startbootstrap-clean-blog/notebook.html"
    #with open(htmlFile, 'w') as f:
    #    f.write(h)


    testOne()

    #test2()



