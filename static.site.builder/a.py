import markdown

import pathlib
import re

fileName = "../about.md"
fileName = "../notebook.history.md"

text = pathlib.Path(fileName).read_text(encoding='utf-8')

md = markdown.Markdown(extensions=['meta', 'toc'])
md.convert(text)
print(md.Meta)


# find header with regular expression

def findHeader(text, search=''):
    #r = re.compile(r"(?<!#)# " + search + r"(?s)(?:(?!(?<!#)# ).)+")
    r = re.compile(r"^#+\s+\w+\n")
    return(r.findall(text))


# use this one
def getHeaders(text):
    r = re.compile(r"^#+\s+\w+\n", re.MULTILINE)
    all = r.findall(text)
    return all

print(findHeader(text))

'''
r"          # raw string
    (?<!#)      # negative lookbehind, make sure we haven't a # before
    #           # a # and a space
"           # end string
+           # concat
    search      # header to be searched
+           # concat
r"          # raw string
    (?s)        # . matches newline
    (?:         # non capture group (Tempered greedy token)
        (?!         # negative lookahead, mmake sure we haven't after:
            (?<!#)      # negative lookbehind, make sure we haven't a # before
            #           # a # and a space
        )           # end lookahead
        .           # any character including newline
    )+          # end group, may appear 1 or more times
"           # end string
'''


if __name__ == "__main__":
    t3 = text[:300]
    hs = getHeaders(t3)
    print(hs)

    import mda

    pass

