
Plywood production requires a good log, called a peeler, 
which is generally straighter and larger in diameter than 
one required for processing into dimensioned lumber by a sawmill. 
The log is laid horizontally and rotated about its long axis while
a long blade is pressed into it, 
causing a thin layer of wood to peel off 
(much as a continuous sheet of paper from a roll). 
An adjustable nosebar, which may be solid or a roller, 
is pressed against the log during rotation, 
to create a "gap" for veneer to pass through between 
the knife and the nosebar. 
The nosebar partly compresses the wood as it is peeled; 
it controls vibration of the peeling knife; 
and assists in keeping the veneer being peeled to an accurate thickness. 
In this way the log is peeled into sheets of veneer, 
which are then cut to the desired oversize dimensions, 
to allow it to shrink (depending on wood species) when dried. 
The sheets are then patched, graded, 
glued together and then baked in a press at a temperature of at least 140 °C (284 °F), 
and at a pressure of up to 1.9 MPa (280 psi) (but more commonly 200 psi) 
to form the plywood panel. The panel can then be patched, 
have minor surface defects such as splits or small knot holes filled, 
re-sized, sanded or otherwise refinished, 
depending on the market for which it is intended.

Plywood for indoor use generally uses the less expensive urea-formaldehyde glue, 
which has limited water resistance, 
while outdoor and marine-grade plywood are designed to withstand moisture, 
and use a water-resistant phenol-formaldehyde glue to 
prevent delamination and to retain strength in high humidity.

Video Credit: www.westfraser.com
