
Chinese Wealthy Families Grow Despite Multiple Uncertainties
By Fang Block Updated March 10, 2023 3:35 pm ET
Order Reprints
Print Article


Beijing has the largest number of wealthy families in China.
Wong Zihoo/Unsplash
Text size

The number of affluent families in China and their combined wealth has continued to grow despite the Covid-19 pandemic and economic and political uncertainties.

About 5.2 million Chinese families had a total wealth of 6 million yuan (US$900,000) or more as of January 2022, up 2.1% year over year. Their combined wealth grew 2.5% year over year to 164 trillion yuan, according to a report by Hurun Research Institute released Friday.

It’s noteworthy that the report is based on data from 2021, covering China’s mainland, Hong Kong, and Taiwan. In 2022, real estate markets and stock markets across the Greater China area all experienced a downturn.

The number of ultra-high-net worth families in China, or those with 100 million
yuan in total wealth, increased 3.5% or 4,600 households to 138,000 households.


Total wealth includes investable assets such as equities, funds and cash, investable properties, as well as a family’s main residence and privately-owned businesses, according to Hurun, which is often compared to Forbes in China.

Measured by investable assets alone, the number of Chinese households that had
more than 100 million yuan reached 82,000.

The main reason for the wealth growth “is the real estate market, especially in first-tier cities,” Rupert Hoogewerf, chairman and chief researcher of Hurun Report, said in a statement. “On the other hand, the domestic stock market has also boosted the number of affluent households.”


Nevertheless, 2021 was a challenging year for many wealthy families in China,
mainly because of the resurgence of the Covid-19 pandemic. The wealth growth
was a sign of “strong resilience in their demand for asset preservation and
appreciation,” according to the report, which is published in collaboration
with Yicai, a Qingdao-based wealth management firm.

Affluent households concentrated in large and coastal cities, with the top 30
largest cities accounting for 68% of the total.

Beijing led with 20,400 families with at least 100 million yuan, followed by
Shanghai with 17,500 ultra-affluent families. Shenzhen and Guangzhou ranked
third and fourth with 5,990 and 4,690 ultra-affluent families, respectively.
