
# 2023 10 17

占有土地会得到厄运， 如果不相信的话就看一看这一段历史。 
进一步请问：占有财富可以得到好运吗？ 
占有权力可以得到好运吗？  什么都不占有， 
谨守中庸可以得到好运吗？子孙满堂是好运吗？
生在中国， 哪里有什么好运？

即没有智力去数清死人，
也没有勇气去做个正常人。

对于人民解放军解放了全中国之后，
站起来的中国人民的死亡率的问题提前校对一下计数背景，
顺便我们也为中共学者们提供一个机会，如果我们站到它们的立场，
如何嘲弄作者这样死脑筋的对伟大的中国革命史非要强调死亡标准的立场？
比如说：

-

这里是血祭人类的现场，极权社会愚民们的基本特点是结构赋予的，
人与人之间是不可回避的永远的恶意，

杀人才可以快意恩仇， 杀人才 可以解决问题， 
杀人是解决最终的所有的问题的手段， 如果上帝同意的话。

-

观察一下中国的猩猩们用身体语言讲述的政治学吧， 
在600万年前， 政治就不是什么新鲜玩意儿。 猩猩眼中，
一切复杂的政治其实简单清楚， 身体语言是所有语言的根源。 
公有制仿佛是伟大的理论， 为杀人奠定坚实的基础， 
抢夺财产因此成为革命工作， 
可是公有财产可以公开吗？
毛泽东的稿费或者伙食费可以公开吗？

杀人的权利是统治者的特权， 是极权， 
而杀人的理由也就令后人瞠目， 令前人结舌， 
用阶级斗争的方法建设共产主义， 
这竟然被说成是人类的共同理想， 
在杀害中达到极乐世界。

不仅统治者不放弃杀人权， 反抗者也想要杀人。

法制建设成为历史中的笑话， 但是如果有人敢笑出声来， 
他一定是白痴， 他无法理解金字塔上的面孔只允许一张表情： 
砖头一样的面孔。 面无表情的傲慢。 如同金字塔上的一块块石头。

阶级斗争过程并没有消除阶级， 
层层阶级铺成更高的塔， 镇压了良知。
现代社会不应该让一个阶级奴役另一个， 
可是阶级斗争让一个阶级消灭另一 个。
在没有言论自由的国家， 理智就是这样的荒唐。

在这一座金字塔上， 唯一的方向就是向上。 
而在攀爬能力上， 人类远不是 猩猩的对手。 
爬上去的人形形色色， 在雄伟的塔上失去人性， 
他们不知道自己的信念 就是这一座高塔， 
用全部生命向上爬， 宝塔就是信仰， 
中国人民不知道自己生活的体制超越人性和生命，
超越神话和传说。


-


事实上相反，在历史上两者顺序出现，在理论基础上两者并不矛盾，
在捍卫人类道德的战场上，对神的信仰让人们能够超越自我的罪欲而不是向畜生堕落，
耶酥完全是一个人，也正是热爱耶酥的人们创造了更多科学技术的内容。
阅读圣经的人们建立了逻辑更完善的神学理论，
我们并不是说所有宗教都以圣经为经典，
但是圣经阅读者们在历史上展示了一个很好的例子，
虽然在一定程度上是个孤例。

然而人类的演化至今仍然是个孤独的奇遇。
当我们说人类史的时候，
我们必须说明这不是至今为止多数中国人对历史的理解，
历史即不等于历史书的集合，也不可能由胜利者书写。
历史至少是四维时空中的存在，可以被科学研究，它的主角是人类。

在人类史中各种创世纪的科学与宗教理论中大爆炸属于容易被学者们认同的一种，
但是它难于证伪，并不能被归属于实证的科学。

宇宙边缘如果存在的话，它可能是以超越光速的速度远离观测者，
也就是说在光速限制中人类不可能看到边缘传来的任何光，
那么我们如何知道宇宙的边缘在哪里？

-

在逃跑的女人身后是一个失去人性的国家。人们找不到天堂，
却可以制造地狱， 称之为走向天堂的台阶。


如果人类以为爬出地狱可以变得坚强， 就再拉回来一次。 

//to rm
甲骨文被发明三千年后， 
这群东西以马克思、恩格斯、列宁的名义统治着神州大地， 
搞死无可计数的同胞， 中共人却还自认文明， 
仿佛文明是一笔遗产， 这种文字真是令得鬼夜哭。


毛泽东和它身边的人们的淫秽和无知，
它们可以为所欲为而且无人敢于 LOOK，
以至于后来毛泽东对中国文化都产生了鄙视，
它甚至需要发动一个文化大革命。

「金字塔内目光的定向」一方面刺激人们向上爬，
一方面将爬上去的人们更彻底的变为爬虫类。

因此金字塔的顶点和黑洞遵守相反的原理，
黑洞禁止光线离开，而塔顶禁止目光射入。

-

每个人想都不想的都会去践踏每个人。而这一切真相，
包括几何化的社会分析方法在中文里从未出现过，
因为自古以来这里就是愚民社会，是一个大一统的奴隶营。
生而为奴的不仅仅是今天被饿死的农民，而且是历史上皇帝的子孙。
皇帝本人可以说：我死后哪管它洪水滔天。
当它们这样想、这样说的时候已经退化为爬虫国王，


不应该把珠宝丢给猪，
可是我们已经把金字塔丢给了爬虫们。
请注意共产党真的是中国人民的代表， 
毒虫们就是爬虫类的代表，
一种能够让邻居饿死都发不出声音的物种实在值得人们警惕。

一些人辩解说这是天灾， 饥荒不能算做杀害。错了，
在一个拥有电报、电话、汽车、轮船和飞机的地区， 
而且没有发生战争， 饥荒就是一种很残忍的杀害。


- 2023 10 21

因此不仅中共必亡，而且中国必亡。
亡的是国，而不是人。
亡的是血祭人类的权力金字塔，
塔上的爬虫们将会因此发现自己原来是人类，
当觉醒的人数超过爬虫的数目时，黑三角将开裂。
黑暗祭台破裂的时候，
更多人得到自由与真实的生命，
得到上帝赋予的创造力与人性之爱。

# 2022 05 05

这些工作是一些值得信任的学者们做的，
在科学的基础上严谨的推算着宇宙的起源。
这时候不可能有人，可是
虽然还不知道思想的来源，
但是很清楚的可以感觉到我们的思想在此时达到了宇宙的尽头，
达到了没有生命的极点。

当我们「看」到具体的高能演化，
我们就不再满足于曾经肤浅的智慧了，
我们就不自以为是的讲什么一沙一世界了，
世界的数目是不是多如恒河沙数之类的问题不再是我们所关心的。
物理学研究提供了更精美的世界起源模型，
并且研究还在持续进步，世界的维度可能高达 11 维，
在大爆炸起源之外还有静态宇宙、平行空间等理论，而且
后世的科学工作者还将将会贡献更多我们无缘了解的智慧，
所以说，我们无法 100% 确定大爆炸是一定正确的世界起源。

Big Bang，本身更像是一个神迹。

请问什么是神？

这个问题虽然难有标准答案，可是也不算是很难理解。另外，
如果有读者可以把其它的世界起源的科学模型焊接在人类史之前，
我们不会介意，我们不是愚民，我们可以理解神学，也可以理解科学。

科学的基础是逻辑，逻辑是思维的法则，
在理性的思维中，至少我们应该有能力做到：

Yes for yes, No for no.

在讲故事之前先说明思想的原则，也展示科学的基础，
避免中共人对科学的无耻利用被我们的读者习以为常的接受。

我们应该即有能力证明，也有能力证伪。
我们的故事不是以暴力来洗脑，而是尽可能的分享。
我们即可以寻找信仰，也可以否定错误。

这个故事中我们尝试分享宏观的时间长河的轮廓，
这样，逃亡者有更多机会找到逃亡的方向。
为了让故事变得简单，
我们在之后的138亿年里都将遵守时序。

大爆炸起源的依据有：
宇宙中存在微波背景辐射，
遥远星系之间的空间在扩展，
按照哈勃定律存在均匀的红移，
而且根据广义相对论所做的计算也符合大爆炸模型，
根据大爆炸推导出来的原始物质的丰度也符合天文观测数据。

深入的了解大爆炸需要更多物理学方面的知识，读者请自行学习。

不知道这个原始的「点」有没有可能存在一个稳定状态，反正直接爆炸了。

爆炸开始后才有了时间。

百万分之一秒时， 
夸克和胶子结合成为诸如质子和中子的重子族。 
之后宇宙的温度开始从极点降低， 
不足以产生新的质子－反质子对、中子－反中子对。 
从而导致了粒子和反粒子之间的质量湮灭， 原
有的质子和中子仅有 十亿分之一保留下来， 
而对应的所有反粒子则全部湮灭。

参照高能物理实验的观测结果人们可以有限的分析此时的情况。

但是不应该在大爆炸之外“观察”大爆炸，根据物理模型，
这是唯一的，在此之外没有空间，没有物质，没有位置，也没有时间。

置身在唯一之内是什么感觉呢？
唯一之外不可能有唯二唯三，什么都没有。
我们无法超然的以观赏恒河之沙的眼光来看待世界的起源，
那种眼光不存在，那种眼光只是荒谬的幻觉。

人们只能在思想中「观察」大爆炸。

让我们假设有比思想更原始的意识存在。

如果要为这样的意识赋予拟人化的特征，此时唯一的意识就是：
「没有人，没有什么，没有任何存在能够在我之外看着我，包括神。」

我就是神，我是起点，我是极点，
我是元始，我是太极，我是一，我的名字是：元一。
-- 你以为你是中国的皇帝吗？十四亿等同愚民的皇帝之一？

我们_如果在-就只能在_元一之内，
从普朗克常数的大小开始。

这就是故事设定的逃亡者历史的起点，这也是时间的起点，
之后的具体细节是历史上其它物理学者们研究出来的，我们只是简述。


## 2022 0505 10:32am 

有一句古文说：太极生两仪，两仪成八卦...  
不清楚光子究竟是在两仪中还是在八卦中。

光子对太极说：我存在了，你就不是唯一。

不知道太极是如何回答的，
当一种存在被拟人化之后，它就产生了情绪。

### 10:33am

我们是以科学探索生命出现之前的时间，
科学的过程，具体的说就是观测事实，测试数据，
以此为依据建立理论，然后寻找理论中的漏洞。


( 科学的基础是逻辑思维，
  逻辑要求人们分辨对与错，
  如果在一个社会中人们无法安全的分辨对与错，
  哪里有科学存身的可能？
  这就是中国史在西学东渐之前没有生长科学大树的原因之一。)


# 2022 05 09

在我们开始进入人类史之前，我们提前说一下，
我们将不会回避神学，当然也会讲解科学，
但是任何一门学问中都有逻辑，当然也有常识，
我们不会如同愚民一般假设神学中没有逻辑，或者神学一定和科学矛盾。


# 2022 05 10

什么时候把战争称为战争？
而不是 SOB 的特别军事行动。
什么时候反纳粹的大阅兵好像是纳粹们在走鹅步？
教你一个单词叫做 Cannon Fodder，
因为无法自由走路的人没有自由的身体所以这些人也不可能拥有自由的心，都将变成为炮灰。
他们更不知道自由的思想必须要有自由的语言，
所以战争不可以被称为战争，
而入侵就被变成了解放。

写下一万条理由你就万分有理吗？
全世界都反对你，所以要再来一次？
俄罗斯人什么时候能恢复理智？
能不能理解人类社会第一也是最后的任务是：数清死人。

它们永远数不清。

俄罗斯可以恶化成为中国。

什么时候可以把防疫称为愚民，
把清零称为灭智，把包子称为毛主席？

什么时候铁链不是铁链，
而是人人想要的脖子上的大金链子。
什么时候奴隶们自己戴上铁链好像皇帝穿上新装，
裸奔也算是时装秀？

什么时候红卫兵变成了白卫兵，义和拳变成了小粉红，
什么时候阴阳颠倒，人一定要阴了才算是人？

为什么你说我阴了我才能阴着，你说我阳我就羊？
你测定我是不是羊的方法可靠不可靠？

如果我阳了却没有死，算不算你防疫失败，还是说我应该去死？

究竟是病毒坏还是你们坏？

因为你们讲得出一万条理由所以就万分有理？

为什么你有五千年历史别人就都是太空人丢到地球上的垃圾？
不是中国人就没有历史也不知道这些人是哪里来的？

为什么我说句实话就犯了法，就变成了西方的搅屎棍？
为什么全人类那么多国家就都欠着你的要让你崛起？

为什么地球人一碰到你们就变成斗地主中的地主？
为什么你们一肚子病毒却还要到我家里来消毒？
为什么你们满世界跑却要把我关起来？

让你们连任终身直到世界末日，中国和俄国都送给你们，你们可不可以不要闹了？

战争叫什么都随你们便，反纳粹的特别军事行动或者防疫和清零，
还有你们可以发明的任何符号... ZZZZZ zzzzz zz z .....


# 2022 05 12

所有的雪都是人造的，
滑雪道在冬季的荒山上远远得像是几条白线，
另一个竞赛场就在废弃工厂旁，
高台滑雪的镜头中很容易出现高高的旧烟筒，
这实在缺少冰雪的美感。


边境农场


  各国援助乌克兰武器列表： 
  https://en.wikipedia.org/wiki/List_of_foreign_aid_to_Ukraine_during_the_Russo-Ukrainian_War

现在普京处于什么状态？
